;;; We use a weight-balanced tree to maintain a dictionary of dynamic
;;; (identifier known only at runtime) bindings.
(load-option 'wt-tree)
(define symbol-wt-type (make-wt-tree-type symbol<?))
;;; The dynamic environment is a map from symbols to values, and at the
;;; top level it is empty.
(define dynamic-environment (make-wt-tree symbol-wt-type))

;;; (with-dynamic-identifiers ID-WT-TREE expr) allows (dq a) within the
;;; expr to find dynamic bindings for a. This form can be nested and uses
;;; lexical scoping. Effectively, it is a (Reader symbol-wt-type) monad with syntactic sugar.
(define-syntax with-dynamic-identifiers
  (sc-macro-transformer
   (lambda (exp env)
     (let ((old-env-id (close-syntax 'dynamic-environment env))
           (new-env-expr (close-syntax (cadr exp) env))
           (block (make-syntactic-closure env
                                          '(dynamic-environment dynamic-variable-access)
                                          (caddr exp))))
       ;; order of arguments to union allows the new environment to
       ;; shadow bindings in the old one, let* is why we have lexical
       ;; scope.
       `(let* ((dynamic-environment (wt-tree/union ,old-env-id
                                                   ,new-env-expr))
               ;; sym is the symbol we want to find a dynamic
               ;; binding for
               (dynamic-variable-access
                (lambda (sym #!optional condition)
                  (call-with-current-continuation
                   (lambda (k)
                     (with-restart 'use-value
                                   (string-append "Specify a value to use instead of "
                                                  (symbol->string sym)
                                                  ".")
                                   (lambda (new-val) (k new-val))
                                   #f ; no interactor
                                   (lambda ()
                                     (with-restart 'store-value
                                                   (string-append "Define "
                                                                  (symbol->string sym)
                                                                  " to a given value.")
                                                   (lambda (new-val)
                                                     (begin
                                                       (set! dynamic-environment
                                                             (wt-tree/add dynamic-environment
                                                                          sym
                                                                          new-val))
                                                       (k new-val)))
                                                   #f
                                                   (lambda ()
                                                     (if (wt-tree/member? sym dynamic-environment)
                                                         (k (wt-tree/lookup dynamic-environment sym 'error))
                                                         (error
                                                          (if (default-object? condition)
                                                              (make-condition
                                                               condition-type:unbound-variable
                                                               (call-with-current-continuation (lambda (x) x))
                                                               (list (find-restart 'use-value)
                                                                     (find-restart 'store-value))
                                                               (list 'location sym 'environment dynamic-environment))
                                                              condition)))))))))))) 
          ;; Now we give a condition handler which checks if
          ;; statically unbound variables are dynamically bound.
          (bind-condition-handler (list condition-type:unbound-variable)
                                  (lambda (condition)
                                    (let ((unbound-id
                                           (access-condition condition 'location)))
                                      (use-value (dynamic-variable-access unbound-id condition) condition)))
                                  ;; Must be a thunk for bind-condition-handler
                                  (lambda () ,block)))))))

#|
(define a 'x)
(define x-tree (alist->wt-tree symbol-wt-type (list (cons 'x 4))))
(define x-tree-other (alist->wt-tree symbol-wt-type (list (cons 'x 'other-value))))
(define y-tree (alist->wt-tree symbol-wt-type (list (cons 'y 'y-value))))
(with-dynamic-identifiers x-tree (dynamic-variable-access a))
;;;Value: 4
(with-dynamic-identifiers x-tree ((lambda (a) a) x))
;;;Value: 4
(with-dynamic-identifiers x-tree x)
;;;Value 175: 4

;;; Lexical Scoping:
(with-dynamic-identifiers x-tree
 (with-dynamic-identifiers x-tree-other x))
;;;Value: other-value

(define (wrap-value-nop v)
  (with-dynamic-identifiers y-tree v))
(wrap-value-nop y)
;;;Unbound variable: y
(define y-processor (lambda (callback)
                      (callback (with-dynamic-identifiers y-tree y))))
(y-processor pp)
;;;y-value
|#
